## Docker containers are instances of a image and docker images make the instances


### Install docker Community edition !#
 

 *! you can follow the steps in the link

   https://docs.docker.com/install/linux/docker-ce/ubuntu/ !*


#After installation just check wheather docker CE* is installed correctly !! So

 open up your terminal 

#sudo docker run hello-world / docker run hello-world( if you can run docker

 commands without sudo) , If docker is installed correctly it would show like

 this 


#Make your first pull here i’am using

  docker pull ubuntu:18.04 

#Now to  check for docker  images command
 
  docker images


This command lists the images available!!





#Now its time to containerize a ubuntu 18.04 box

  docker run -it ubuntu:18.04




This is a container running ubuntu 18.04 box 



#Now to check currently running container ,open other terminal (ctrl + Alt T)

 
  docker ps  



*To check or view all containers command : docker ps -a  *


#Now to stop a running instance 

 docker stop container-id/tagname 

eg.. docker stop f9315d059527


#To start a container first check 

 docker ps -a (list all containers)

#out of which you can start a container you need 

 docker start containerid / tagname

eg... docker start f9315d059527 

#but this only starts a container but doesn’t make it interactive for eg 


# Now to make it interactive 
  
  docker attach containerid / tagname

eg.. docker attach f9315d059527
 

# Now this is interactive !!! 
